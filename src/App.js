import logo from './logo.svg';
import './App.css';
import Demoheaderlayout from './layoutthuchanh/Demoheaderlayout';
import Content from './layoutthuchanh/Content';
import Item from './layoutthuchanh/Item';
import Footer from './Footer';

function App() {
  return (
    <div className="App">
        <Demoheaderlayout></Demoheaderlayout>
        <Content/>
        <Item/>
        <Footer/>
    </div>
  );
}

export default App;
